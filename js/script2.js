let m=prompt('Input starting number of the range'),
    n=prompt('Input ending number of the range'),
    i, j;

while (n < m || isNaN(m) || isNaN(n)) {
    alert('Error! You entered not the correct number or range!');
    m=prompt('Input starting number of the range');
    n=prompt('Input ending number of the range');
}
nextIter:
    for (i = m; i <= n; i++) {

        for (j = 2; j < i; j++) {
            if (i % j === 0) {
                continue nextIter;
            }
        }

        console.log( i );
    }